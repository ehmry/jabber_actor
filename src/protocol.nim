
import
  std/typetraits, preserves

type
  XmppClient* {.preservesRecord: "xmpp-client".} = object
    `jid`*: string
    `password`*: string
    `cap`* {.preservesEmbedded.}: Preserve[void]

proc `$`*(x: XmppClient): string =
  `$`(toPreserve(x))

proc encode*(x: XmppClient): seq[byte] =
  encode(toPreserve(x))
