# Package

version = "20230424"
author        = "Emery Hemingway"
description   = "Jabber Syndicate actor"
license       = "Unlicense"
srcDir        = "src"
bin           = @["jabber_actor"]


# Dependencies

requires "nim >= 1.6.10"
